const users = [
    {
        id: 'u01',
        firstName: 'John',
        lastName: 'Doe',
        birth: '5.05.1995',
    },
    {
        id: 'u02',
        firstName: 'Jane',
        lastName: 'Doe',
        birth: '6.06.1996',
    },
    {
        id: 'u03',
        firstName: 'Jack',
        lastName: 'Doe',
        birth: '7.07.1997',
    },
]
