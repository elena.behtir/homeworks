const orders = [
    {
        id: 'o01',
        userId: 'u03',
        contains: [
            {
                productId: 'pr02',
                amount: 2
            },
            {
                productId: 'pr01',
                amount: 3
            }
        ]
    },
    {
        id: 'o02',
        userId: 'u01',
        contains: [
            {
                productId: 'pr01',
                amount: 4
            },
            {
                productId: 'pr03',
                amount: 2
            },
            {
                productId: 'pr02',
                amount: 6
            }
        ]
    },
    {
        id: 'o03',
        userId: 'u02',
        contains: [
            {
                productId: 'pr02',
                amount: 3
            },
            {
                productId: 'pr03',
                amount: 5
            }
        ]
    },
];
