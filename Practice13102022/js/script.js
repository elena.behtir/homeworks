// const data = users;
// const field = `birth`;
// const range = `desk`;

const data = products;
const field = `price`;
const range = `asc`;

// const data = orders;
// const field = `id`;
// const range = `asc`;

function sortProducts() {
    return data.sort((data1, data2) => {
    switch (field) {
        case `price`: 
            if (range == `asc`) {
                return (data1.price - data2.price);
            } else {
                return (data2.price - data1.price);
            }; 
        case `id`: 
            if (range == `asc`) {
                return (data1.id.localeCompare(data2.id));
            } else {
                return (data2.id.localeCompare(data1.id));
            };
        case `name`: 
            if (range == `asc`) {
                return (data1.name.localeCompare(data2.name));
            } else {
                return (data2.name.localeCompare(data1.name));
            };
        case `amount`: 
            if (range == `asc`) {
                return (data1.amount - data2.amount);
            } else {
                return (data2.amount - data1.amount);
            }
        case `description`: 
            if (range == `asc`) {
                return (data1.description.localeCompare(data2.description));
            } else {
                return (data2.description.localeCompare(data1.description));
            }            
        }              
    });
}

// console.log(sortProducts());

function sortUsers() {
    return data.sort((data1, data2) => {
    switch (field) {
        case `id`: 
            if (range == `asc`) {
                return (data1.id.localeCompare(data2.id));
            } else {
                return (data2.id.localeCompare(data1.id));
            };
        case `firstName`: 
            if (range == `asc`) {
                return (data1.firstName.localeCompare(data2.firstName));
            } else {
                return (data2.firstName.localeCompare(data1.firstName));
            };
        case `lastName`: 
            if (range == `asc`) {
                return (data1.lastName.localeCompare(data2.lastName));
            } else {
                return (data2.lastName.localeCompare(data1.lastName));
            };
        case `birth`: 
            if (range == `asc`) {
                return (new Date(data1.birth) - new Date(data2.birth));
            } else {
                return (new Date(data2.birth) - new Date(data1.birth));
            }             
        }              
    });
}

// console.log(sortUsers());

function sortOrders() {
    return data.sort((data1, data2) => {
    switch (field) {
        case `id`: 
            if (range == `asc`) {
                return (data1.id.localeCompare(data2.id));
            } else {
                return (data2.id.localeCompare(data1.id));
            };
        case `userId`: 
            if (range == `asc`) {
                return (data1.userId.localeCompare(data2.userId));
            } else {
                return (data2.userId.localeCompare(data1.userId));
            };  
        }               
    });
}

function sort(data) {
    if (data === products) {
        return sortProducts();
    } else {
        if (data === users) {
            return sortUsers();
        } else {
            return sortOrders();
        }
        
    }
}

console.log(sort(data));
