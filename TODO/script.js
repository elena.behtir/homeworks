
// функція яка рахує кількість входжень підрядку в рядок

//lorem ipsum dolor sit amet dolor ipsum lorem, lo return 4
// lorem ipsum dolor sit amet dolor ipsum lorem -> rem ipsum dolor sit amet dolor ipsum lorem ,1 -> em ipsum dor sit amet dolor ipsum lorem, 2 -> em ipsum dor sit amet dor ipsum lorem, 3 -> em ipsum dor sit amet dolor ipsum rem, 4 -> 4


// function stringCut(str,index,amount) {
//     return str.slice(0,index) + str.slice(index+amount);
// }



function stringCut(str,index,amount) {
    return str.slice(index+amount);
}


function indexOfAmount(str,substr) {
    let currentString = str;
    let counter = 0;

    while (currentString.indexOf(substr)>(-1)) {
        counter++;
        currentString = stringCut(currentString,currentString.indexOf(substr),substr.length);
    }
    return counter;
}
// debugger;
console.log(indexOfAmount('lorem ipsum dolor sit amet dolor ipsum lorem', 'lo'));
console.log(indexOfAmount('lorem ipsum dolor sit amet dololorr ipsum lorem', 'lor'), 'should be 4 but we got 5');
// TODO: пофіксити алгоритм 24.09




//функція яка рахує кількість слів в реченні



function wordsAmmount (sentence) {
    if (!sentence.trim() || +sentence) return 0;
    let counter = 0;
    while (sentence.indexOf(" ") !== -1){
        word = +sentence.slice(0, sentence.indexOf(" "));
        while (isNaN(word)){
            sentence = sentence.slice (sentence.indexOf(" ") + 1).trim();
            return counter;
        }
           // TODO: перевірити чи є перше поточне слово числом і якщо число то не рахувати його за слово 26.09
        counter++;
        sentence = sentence.slice (sentence.indexOf(" ") + 1).trim();
    }
    return ++counter;
}
// debugger;

console.log(wordsAmmount('lorem ipsum dolor sit amet'));
console.log(wordsAmmount('              '));
console.log(wordsAmmount('lorem ipsum    dolor     sit amet'));
console.log(wordsAmmount('123456 khhb ijkn 454'));


// вставити об'єкт з регіонами  з файлу на гіт

const car = {
    model: 'Toyota Camry',
    owner: 'John Doe',
    lisensePlate: 'AE5467HH',
    get region() {
        let letters = this.lisensePlate.slice(0,2);
        for (let key in carLicensePlates) {
            carLicensePlates[key].indexOf(letters);
            if (carLicensePlates[key].indexOf(letters) > -1) {
                return ('${key} oblast');
            }
        }
        return 'erorr';
    }
}


// функція яка приймає об'єкт і повертає пару поле ключ

const testObject1 = {
    a: 5,
    b: 6,
    c: 'lorem',
    d: false,
    e: null
}

const testObject2 = {
    a: 5,
    b: 6,
    c: 'lorem',
    e: null,
    g: true,
}

const testObject3 = {
    a: 5,
    b: 6,
    c: 'lorem',
    d: false,
    e: null
}

function keyValuePairs(obj) {
    for (let key in obj) {
        console.log(`Key: ${key} Value: ${obj[key]}`);
    }
}

keyValuePairs(testObject1);


function removeByKey(obj, keyToRemove) {
    const newObj = {};
    for (let key in obj) {
        if (key !== keyToRemove) {
            newObj[key] = obj[key];      
        }
    }
    return newObj;
}

console.log(removeByKey(testObject1, 'b'));


function removeByKeys(obj) {
    const newObj = { };
    console.log(arguments);
    for (let key in obj) {
        let flag = true;
        for (let i = 1; i < arguments.length; i++) {
            if (key == arguments[i]) {
                flag = false;
                break;  
            }
        }
        if (flag){
            newObj[key] = obj[key]; 
        }
    }    
    return newObj;
}

console.log(removeByKeys(testObject1, 'a', 'c'));

console.log(Object.keys(testObject1));

function isEqual(obj1,obj2) {
    if (Object.keys(obj1).length === Object.keys(obj2).length) {
        for (let key in obj1) {
            if (obj1[key] === obj2[key]) {
                continue;
            } else {
                return false;
            }
        } return true;
    } else {
        return false;
    }
}

console.log(isEqual(testObject1,testObject2));


// перетин двох об'єктів

function intersection(obj1, obj2) {
    const newObject = {};
    for (let key in obj1) {
        if (obj1[key] === obj2[key]) {
            newObject[key] = obj1[key];
        }
    }
    return newObject;
}

console.log(intersection(testObject1, testObject2));