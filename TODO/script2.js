console.log(characters);

function charactersInFilm(filmName) {
    let allCharacters = [];
    allCharacters.forEach((character) => {
        if (character.films.indexOf(filmName) > -1) {
            allCharacters.push(character.name);
        }
    })
    return `${allCharacters.join(", ")} were in ${filmName}`;
}

console.log(charactersInFilm("A New Hope"));

function findCharacterByName(characterName) {
    return characters.find((character) => {
        return character.name === characterName;
    })
}

console.log(findCharacterByName("R2-D2"));


// приклади років "5BBY", "10ABY"
// написати функцію, яка буде приймати ім'я персонажа і рік (по літочисленню далекої галактики) і повернути скілький йому було б років. Якщо рік менший ніж рік народження - повернути "ще не народився"

function characterAge(characterName, year) {
    const character = characterInfo(characterName);
    if (!character) return `${characterName} does not exist at all`;
    if (!character.birth_year === `unknown`) return `age of ${character.name} is unkhown`;
    const normalYear = (year.IndexOf(`BBY`) > -1) ? -parseInt(year) : parseInt(year);
    const normalBirthYear = (character.birth_year.IndexOf(`BBY`) > -1) ? -parseInt(character.birth_year) : parseInt(character.birth_year);
    let age = normalYear - normalBirthYear;
    if (age <= 0) {
        return `in ${year} ${character.name} does not exist`;
    }
    return age;
}

console.log(characterAge(`R2-D2`, `40BBY`))


// вивести всіх пілотів зі списку

function getAllPilotes() {
    const pilots = [];
    characters.forEach((character) => {
        if (character.starships.length) {
            pilots.push(character.name);
        }
    });
    return `All pilots: ${pilots.join(", ")}`;
}

console.log(getAllPilots());


// відсортувати всіх по зросту

function sortHeight() {
    return characters.sort((character1, character2) => {
        return character1.height - character2.height;
    });
}

console.log(sortHeight());


