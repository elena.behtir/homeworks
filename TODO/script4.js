// функція яка розкладає число на множники
// 120 = "2^3*3*5"

// function factorization(num) {
//     let result = "";
//     for(let i = 2; i <= num; i++) {
//         let power = 0;
//         while (!(num % i)) {
//             num = num / i;
//             power++;
//         }
//         if (!power) {
//             continue;
//         }
//         if (!result) {
//             result = result + ((power > 1) ? `${i}^${power}` : `${i}`);
//         } else {
//             result = result + ((power > 1) ? `*${i}^${power}` : `*${i}`);
//         }
//     }
//     return result;
// }
//
// debugger;

// console.log(factorization(120));


// функція яка знаходить НСД двох чисел

// НСД(16,12) = 4

// TODO optional: за допомогою factorization() знайти спільні дільники в розкладі 26.09

// НСД(a,b) = НСД(a%b,b) ... = НСД(i,0) = i, НСД(j,1) = 1

// НСД(16,12) = НСД(12,4) = НСД(4,0) = 4


// НСД(12,16) = НСД(16,12)

function gcd(a,b) {
    while(b>1){
        let tempa = b;
        let tempb = a % b;
        a = tempa;
        b = tempb;
    }
    if (b == 1) {
        return 1;
    } else {
        return a;
    }
}


//
// console.log(gcd(16,12));
// console.log(gcd(19,21));

// debugger;

// console.log(gcd(101, 523));


// TODO optional: за допомогою НСД знайти НСК: НСК(a,b) = (a*b)/НСД(a,b) 26.09




const testObject1 = {
    a: 5,
    b: 6,
    c: 'lorem',
    d: false,
    e: null
};

const testObject2 = {
    a: 5,
    b: 6,
    c: 'lorem',
    e: null,
    g: true
};

const testObject3 = {
    a: 5,
    b: 6,
    c: 'lorem',
    d: false,
    e: null
};


function keyValuePairs(obj) {
    for (let key in obj) {
        console.log(`Key: ${key} value: ${obj[key]}`);
    }
}

//напишіть функцію яка приймає в аргументах обʼект та ключ і повертати новий обʼєкт копію основного обʼєкту без поля з заданим ключем

keyValuePairs(testObject1);

function removeByKey(obj, keyToRemove) {
    const newObj = { };
    for (let key in obj){
        if (key !== keyToRemove){
            newObj[key] = obj[key];
        }
    }
    return newObj;
}

console.log(removeByKey(testObject1, 'b'));

// узагальнити попередню функцію для будь-якої кількості ключів

function removeByKeys(obj) {
    const newObj = { };
    // TODO: зробити перевірку чи є взагалі ключі на видалення 3.10
    for (let key in obj){
        let flag = true;
        for (let i = 1; i < arguments.length; i++) {
            if (key === arguments[i]) {
                flag = false;
                break;
            }
        }
        if (flag){
            newObj[key] = obj[key];
        }
    }
    return newObj;
}
// debugger;
console.log(removeByKeys(testObject1, 'a', 'c'));


const testProduct = {
    name: 'Apple MacBook Air 13"',
    price: '43000',
    properties: {
        display: '13.3" Retina (2560x1600)',
        chip: 'Apple M1',
        ram: '8GB',
        ssd: '256GB',
        os: 'MacOS Big Sur',
        color: 'gold'
    }
}

function ourDate(dateString) {
    const day = dateString.split('.')[0];
    const month = dateString.split('.')[1];
    const year = dateString.split('.')[2];
    return new Date(year, month - 1, day);
}
// let result = condition1 ? (condition2 ? res1: res2) : (condition2 ? res3 : res4)

function onDiscount(product) {
    let startDate = ourDate(prompt("enter first date"));
    let endDate = ourDate(prompt("enter last date"));
    let discount = +prompt("our discount");
    const now = new Date();
    if(endDate >= now && startDate <= now){
        let salePrice = product.price*(100-discount)/100;
        const actualDiscountDateSeconds = (endDate - now)/1000;
        const actualDiscountDateMinutes = Math.floor(actualDiscountDateSeconds/60)%60;
        const actualDiscountDateHours = Math.floor(actualDiscountDateSeconds/3600)%24;
        const actualDiscountDateDays = Math.floor(actualDiscountDateSeconds/(3600*24));
        return `На ${product.name} зараз діє акційна ціна ${salePrice}грн. ${product.name} will be with discount during ${actualDiscountDateDays === 1 ? `${actualDiscountDateDays} day` : `${actualDiscountDateDays} days`}, ${actualDiscountDateHours === 1 ? `${actualDiscountDateHours} hour` : `${actualDiscountDateHours} hours`} and ${actualDiscountDateMinutes === 1 ? `${actualDiscountDateMinutes} minute` : `${actualDiscountDateMinutes} minutes`}.`;
    //    TODO: перекласти англійский текст на українську і зробити так щоб множина відпрацьовувала коректно (1 день, 2 дні, 5 днів) (можна використати switch з case 1, case 2,3,4 та default)
    //    TODO: зробити так що не показувались 0 днів
    }
    return `Нажаль на ${product.name} зараз немає акцій. Ціна складає ${product.price}грн`;
//    TODO: якщо акція тільки буде, прописати через скільки вона почнеться, якщо акція була, то прописати коли вона закінчилась
//    TODO: написати функцію для виведення скільки ще залишилось часу і використати її в усіх місцях до 10.10
}

console.log(onDiscount(testProduct));

// Apple MacBook Air 13 акційна ціна (ціна отут), дісна ще 5 днів 4 години 23 хвилини
