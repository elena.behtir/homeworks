
// функція яка рахує кількість входжень підрядку в рядок

//lorem ipsum dolor sit amet dolor ipsum lorem, lo return 4
// lorem ipsum dolor sit amet dolor ipsum lorem -> rem ipsum dolor sit amet dolor ipsum lorem ,1 -> em ipsum dor sit amet dolor ipsum lorem, 2 -> em ipsum dor sit amet dor ipsum lorem, 3 -> em ipsum dor sit amet dolor ipsum rem, 4 -> 4


// function stringCut(str,index,amount) {
//     return str.slice(0,index) + str.slice(index+amount);
// }



function stringCut(str,index,amount) {
    return str.slice(index+amount);
}


function indexOfAmount(str,substr) {
    let currentString = str;
    let counter = 0;

    while (currentString.indexOf(substr)>(-1)) {
        counter++;
        currentString = stringCut(currentString,currentString.indexOf(substr),substr.length);
    }
    return counter;
}
// debugger;
console.log(indexOfAmount('lorem ipsum dolor sit amet dolor ipsum lorem', 'lo'));
console.log(indexOfAmount('lorem ipsum dolor sit amet dololorr ipsum lorem', 'lor'));
// console.log(indexOfAmount('lorem ipsum dolor sit amet dololorr ipsum lorem', 'lor'), 'should be 4 but we got 5');
// TODO: пофіксити алгоритм 24.09
// Виконано!!!


//функція яка рахує кількість слів в реченні
// function wordsAmmount (sentence) {
//     if (!sentence.trim() || +sentence) return 0;
//     let counter = 0;
//     while (sentence.indexOf(" ") !== -1){
//         // TODO: перевірити чи є перше поточне слово числом і якщо число то не рахувати його за слово 26.09
//         counter++;
//         sentence = sentence.slice (sentence.indexOf(" ") + 1).trim();
//     }
//     return ++counter;
// }

// TODO: перевірити чи є перше поточне слово числом і якщо число то не рахувати його за слово 26.09
// Виконано!!!
function wordsAmmount1 (sentence) {
    let counter = 0;
    let newArr = sentence.split(" ");
    console.log(newArr);
    newArr.forEach((word) => {
        if (!isNaN(word)) {
            counter = counter;
        } else {
            counter++;
        }
    });
    return counter;
}

console.log(wordsAmmount1('lorem  123 ipsum 456 dolor sit amet'));
console.log(wordsAmmount1('              '));
console.log(wordsAmmount1('lorem ipsum  12  dolor     sit amet'));
console.log(wordsAmmount1('123456'));



// функція яка приймає одне слово і першу літеру робить великою

// function capitalizeWord (word) {
//     let firstLetter = word.charAt(0);
//     let newWord = firstLetter.toUpperCase() + word.slice(1);
//     return newWord;
//     //return word.charAt(0).toUpperCase() + word.slice(1);
//     // TODO: переписати на => 26.09
// }

// console.log(capitalizeWord('lorem'));

const capitalizeWord = word => 
    word.charAt(0).toUpperCase() + word.slice(1);

console.log(capitalizeWord('lorem'));  
// Виконано!!!


// функція яка міняє першу літеру рядку на знак оклику

// function firstLetterChange(str) {
//     // TODO: переписати на тернарку
//     // TODO: переписати на стрілкову ф-цію
//     if (typeof str == 'string') {
//         return "!" + str.slice(1)
//     }
// }

const firstLetterChange = str => 
    typeof str == 'string' ? ("!" + str.slice(1)) : str;

console.log(firstLetterChange('lorem'));
// Виконано!!!


//функція яка робить всі слова в реченні з великої літери

// function capitalizeEachWord (sentence) {
//     // TODO: написати перевірку на порожній рядок та на число 26.09 ВИКОНАНО!!!
//     if (!isNaN(sentence)) return `Error!`;
//     // так якщо тільки на порожній рядок
//     // if (sentence.trim().length === 0) return `erorr empty`;
//         let newSentence = "";
//     // TODO: спробувати переписати цикл while на do {} while 26.09
//             while (sentence.indexOf(" ") !== -1) {
//                 let word = sentence.slice (0, sentence.indexOf(" "));
//                 newSentence += capitalizeWord(word) + ' ';
//                 sentence = sentence.slice (sentence.indexOf(" ") + 1).trim();
//             }
//         return newSentence + capitalizeWord(sentence);
// }

// console.log(capitalizeEachWord('2423'));
// console.log(capitalizeEachWord('Lorem lorem ipsum dolor'));
// console.log(capitalizeEachWord(" "));

// ВИКОНАНО!!! Цикл у функції переписаний на do {} while
function capitalizeEachWord (sentence) {
    if (!isNaN(sentence)) return `Error!`;
        let newSentence = "";
    // TODO: спробувати переписати цикл while на do {} while 26.09
        do {
            let word = sentence.slice (0, sentence.indexOf(" "));
            newSentence += capitalizeWord(word) + ' ';
            sentence = sentence.slice (sentence.indexOf(" ") + 1).trim();
        } while (sentence.indexOf(" ") !== -1) 
    return newSentence + capitalizeWord(sentence);
}

console.log(capitalizeEachWord('2423'));
console.log(capitalizeEachWord('Lorem lorem ipsum dolor'));
console.log(capitalizeEachWord(" "));
