const testProduct = {
    name: 'Apple MacBook Air 13"',
    price: '43000',
    properties: {
        display: '13.3" Retina (2560x1600)',
        chip: 'Apple M1',
        ram: '8GB',
        ssd: '256GB',
        os: 'MacOS Big Sur',
        color: 'gold'
    }
}


function ourDate(dateString) {
    const day = dateString.split(`.`)[0];
    const month = dateString.split(`.`)[1];
    const year = dateString.split(`.`)[2];
    return new Date(year, month-1, day);
}

let startDate = ourDate(prompt(`Enter first date`));
let endDate = ourDate(prompt(`Enter last date`));
let discount = +prompt(`Our discount`);

function onDiscount(product) {
    const now = new Date();
    if (endDate >= now && startDate <= now) {
        let salePrice = product.price * (100 - discount) / 100;
        const actualDiscountDateSeconds = (endDate - now) / 1000;
        const actualDiscountDateMinutes = Math.floor(actualDiscountDateSeconds / 60) % 60;
        const actualDiscountDateHours = Math.floor(actualDiscountDateSeconds / 3600) % 24;
        const actualDiscountDateDays = Math.floor(actualDiscountDateSeconds / (3600 * 24));
        return `${product.name} зараз діє акційна ціна ${salePrice} грн. ${product.name} will be with discount during ${actualDiscountDateDays === 1 ? `${actualDiscountDateDays} day` : `${actualDiscountDateDays} days`}, ${actualDiscountDateHours === 1 ? `${actualDiscountDateHours} hour` : `${actualDiscountDateHours} hours`} and ${actualDiscountDateMinutes === 1 ? `${actualDiscountDateMinutes} minute` : `${actualDiscountDateMinutes} minutes`};
    }
    return (`На жаль на ${product.name} зараз немає акцій. Ціна складає ${product.price} грн`);
}

console.log(onDiscount(testProduct));

